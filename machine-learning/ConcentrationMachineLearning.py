"""
Contains methods to
1. carry out 5-fold cross validation on train set
2. save specified model
3. load specified model
4. conduct grid search

These options are toggled using the ml_type variable.
"""

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.neural_network import MLPClassifier
from sklearn.svm import LinearSVC, SVC
from pandas import DataFrame
from enum import Enum
import csv, pickle, time
import numpy as np
import math

# example variables that represent the name of the files to be used
filename_1 = "subject1"
filename_2 = 'subject2'
filename_3 = 'subject3'
filename_4 = 'subject4'
filename_5 = 'subject5'
filename_6 = 'subject6'
filename_7 = 'subject7'
filename_8 = 'subject8'
filename_9 = 'subject9'
filename_10 = 'subject10'
filename_11 = 'subject11'
filename_12 = 'subject12'
filename_13 = 'subject13'
filename_14 = 'subject14'


class MLTypeEnum(Enum):
    # carry out 5-fold cross validation on train set, and also verify on test set
    NORMAL = 0
    # save specified model
    SAVE_MODEL = 3
    # load specified model
    LOAD_MODEL = 4
    # conduct grid search
    GRID_SEARCH = 5


class NormalisationType(Enum):
    NONE = 0
    STANDARD = 1
    MIN_MAX = 2


# TO TOGGLE
# specify which normalisation type to use (none, standard, or min max)
NORMALISATION_TYPE = NormalisationType.NONE
# specify which function to run
ml_type = MLTypeEnum.GRID_SEARCH
# specify which classifiers to use
classifiers_to_use = [MLPClassifier(), LinearSVC(), SVC()]

# For SVM grid search
C_range = np.logspace(-1, 3, 5)
gamma_range = np.logspace(-3, 1, 5)
param_rbf_grid = [dict(gamma=gamma_range, C=C_range, kernel=['rbf'])]
param_poly_grid = [dict(C=C_range, degree=(2, 3, 4, 5), kernel=['poly']), dict(C=C_range, kernel=['linear'])]
# For random forest grid sesarch
num_features = 10
param_rf_grid = [dict(n_estimators=[100],
                      max_features=[math.ceil(len(num_features) / 4), math.ceil(len(num_features) / 2),
                                    math.ceil(len(num_features) / 4 * 3), len(num_features),
                                    math.ceil(math.sqrt(len(num_features)))])]
# Specify classifiers and parameters to use in grid search
grid_clf = RandomForestClassifier()
tuned_parameters = param_rf_grid

concentrate_value = 1
inattention_value = 0


def generate_filename(filename):
    return source_directory + filename + '.csv'


def read_csv(filename):
    print(filename)
    with open(filename, 'r') as dest_f:
        data_iter = csv.reader(dest_f, delimiter=',', quotechar='"')
        final_data = [data for data in data_iter]
        return final_data


def extract_features_and_target(data):
    """
    Extracts the features and targets from each row of the data

    Parameters
    ----------
    data : list of lists
        Contains rows of features and target value
        Target value is at index 0 of each row, and features are the remaining values of each row

    Returns
    -------
    list of lists
        list of features. each row consists of the features of 1 sample
    numpy array
        corresponding target values. each row is the target value of 1 sample
    """
    # ignore contact quality
    m_features = []
    m_targets = []
    for line in data[1:]:
        phase = line[0]
        if phase == 'concentrate':
            m_targets.append(concentrate_value)
        else:
            m_targets.append(inattention_value)
        row_features = line[1:]
        m_features.append(row_features)
    return m_features, np.array(m_targets)


def calculate_scores(m_features, m_targets, m_test_features, m_test_targets,
                     classifiers=classifiers_to_use):
    """
    Calculate the 5-fold cross validation score on the training data, and out-of-sample accuracy on test data for each classfier

    Parameters
    ----------
    m_features: features of training set
    m_targets: target values of training set
    m_test_features: features of test set
    m_test_targets: target values of test set
    classifiers: classifiers (e.g. Random Forest, SVM) to test against

    Returns
    -------
    row of 5-fold CV score and test score from each classifier in order of the classifier in the argument "classifiers"
    """
    m_results = []
    for m_clf in classifiers:
        print(m_clf.__class__.__name__)
        print(m_features[0])
        m_clf.fit(m_features, m_targets)
        scores = cross_val_score(m_clf, m_features, m_targets, cv=5)
        test_score = m_clf.score(m_test_features, m_test_targets)
        m_results += [np.mean(scores), test_score]
        print("Average:", np.mean(scores))
        print("Test score:", test_score)
    return m_results


def run_classifier(results_filename, train_list, test_list):
    """
    Extracts features and targets, runs the 5-fold cross validation and test validation, and write results to csv

    Parameters
    ----------
    results_filename: path of output file
    train_list: list of files in training set
    test_list: list of files in test set

    Returns
    -------
    None
    """
    with open(results_filename, 'w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        header = []
        train_features, train_targets = extract_features_and_targets_from_files(train_list)
        test_features, test_targets = extract_features_and_targets_from_files(test_list)
        for clf in classifiers_to_use:
            header += [clf.__class__.__name__ + "_CV", clf.__class__.__name__ + "_Test"]
        csv_writer.writerow(header)
        results = calculate_scores(train_features, train_targets, test_features,
                                   test_targets)
        csv_writer.writerow(results)


def run_grid_search(results_filename, train_list):
    """
    Extracts features and targets, runs grid search and write results to csv

    Parameters
    ----------
    results_filename: path of output file
    train_list: list of files in training set

    Returns
    -------
    None
    """
    train_features, train_targets = extract_features_and_targets_from_files(train_list)
    clf = GridSearchCV(grid_clf, tuned_parameters, cv=5, n_jobs=4)
    clf.fit(train_features, train_targets)
    print("Best parameters set found on development set:")
    print()
    print(clf.best_params_)
    print()
    print("Grid scores on development set:")
    print()
    means = clf.cv_results_['mean_test_score']
    stds = clf.cv_results_['std_test_score']
    for mean, std, params in zip(means, stds, clf.cv_results_['params']):
        print("%0.3f (+/-%0.03f) for %r"
              % (mean, std * 2, params))
    # write pandas dataframe to file
    results_dataframe = DataFrame.from_dict(clf.cv_results_)
    results_dataframe.to_csv(results_filename)


def extract_features_and_targets_from_files(filenames):
    """
    Extract features and targets from individual files, and combines them together into a single set of features and a single set of targets

    Parameters
    ----------
    filenames: list of files containing features and targets

    Returns
    -------
    m_features: features from all files
    m_targets: target values from all files
    """
    m_features = []
    m_targets = []
    for m_filename in filenames:
        raw_data = read_csv(generate_filename(m_filename))
        output_features, output_targets = extract_features_and_target(raw_data)
        m_features.append(output_features)
        m_targets.append(output_targets)
    m_features = np.array(m_features).astype(np.float)
    m_targets = np.array(m_targets).astype(np.float)
    return m_features, m_targets


def scale_features(features):
    """
   Carries out min-max or standard normalisation of features

    Parameters
    ----------
    features: 2D array  of features

    Returns
    -------
    m_features: scaled features
    scale: resulting scaler
    """
    if NORMALISATION_TYPE == NormalisationType.NONE:
        scale = None
    else:
        if NORMALISATION_TYPE == NormalisationType.STANDARD_ALL:
            scale = StandardScaler().fit(features)
        elif NORMALISATION_TYPE == NormalisationType.MIN_MAX_ALL:
            scale = MinMaxScaler(feature_range=(-1, 1)).fit(features)
        features = scale.transform(features)
    return features, scale


def save_model(m_clf_to_use, m_train_list, output_filename, scaler_filename=None):
    """
    Trains and saves trained model and scaler to file

    Parameters
    ----------
    m_clf_to_use: classifier to use
    m_train_list: list of filenames in training set
    output_filename: path to model output
    scaler_filename:  path to scaler output

    Returns
    -------
    None
    """
    train_features, train_targets = extract_features_and_targets_from_files(m_train_list)
    scaled_features, scale = scale_features(train_features)
    m_clf_to_use.fit(scaled_features, train_targets)
    scores = cross_val_score(m_clf_to_use, scaled_features, train_targets, cv=5)
    with open(output_filename, 'wb') as output_file:
        pickle.dump(m_clf_to_use, output_file)
    if not (scaler_filename is None or scale is None):
        with open(scaler_filename, 'wb') as scaler_output_file:
            pickle.dump(scale, scaler_output_file)
    print(scores)


def test_model(m_clf_to_use, m_file_list):
    """
    Tests model on the first row of test file

    Parameters
    ----------
    m_clf_to_use: classifier to use
    m_file_list: list of filenames in test set

    Returns
    -------
    None
    """
    test_features, test_targets = extract_features_and_targets_from_files(m_file_list)
    print("starting prediction")
    current_time = time.time()
    prediction = m_clf_to_use.predict(test_features[0])
    completed_time = time.time()
    print("time taken:", str(completed_time - current_time))
    print(prediction, test_targets[0])


if __name__ == "__main__":
    if ml_type == MLTypeEnum.NORMAL:
        source_directory = "source_directory"
        overall_train_filenames = [filename_1, filename_2]
        overall_test_filenames = [filename_3]
        path_to_result = "path_to_result"
        run_classifier(path_to_result, overall_train_filenames, overall_test_filenames)
    elif ml_type == MLTypeEnum.SAVE_MODEL:
        model_to_use = SVC(C=10, gamma=0.001, kernel='rbf')
        source_directory = "source_directory"
        path_to_result = "path_to_result"
        path_to_scaler = "path_to_scaler"
        save_model(model_to_use, [filename_1], path_to_result, path_to_scaler)
    elif ml_type == MLTypeEnum.LOAD_MODEL:
        path_to_model = "path_to_model"
        source_directory = "source_directory"
        overall_train_filenames = [filename_1, filename_2]
        with open(path_to_model) as saved_model:
            loaded_clf = pickle.load(saved_model)
        test_model(loaded_clf, overall_train_filenames)
    elif ml_type == MLTypeEnum.GRID_SEARCH:
        path_to_result = "path_to_result"
        source_directory = "source_directory"
        overall_train_filenames = [filename_1, filename_2]
        run_grid_search(path_to_result, overall_train_filenames)
    else:
        raise ValueError("ml type is not specified")
