#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A simple experiment that carries out the Psychomotor vigilance task
There are 5 iterations and two phases in each iteration: inattention and concentration
In the inattention phase, a blank screen is shown.
For the concentration phase, a cross will appear at random intervals (between 2 to 10s), and the user needs to click any key on the keyboard once the cross appears
The time at which the user presses the key, as well as the reaction time (amount of time that passed from the presentation of the stimulus to the pressing of the key) are recorded
Entire experiment lasts for 5 minutes

See also: https://en.wikipedia.org/wiki/Psychomotor_vigilance_task
https://www.med.upenn.edu/uep/assets/user-content/documents/Dorrianetal.PVTchapterinKushida2005.pdf

Requires: Python 2.7, Expyriment
See installation information: http://docs.expyriment.org/Installation.html

"""

from expyriment import design, control, stimuli
import random, sys
import time
import subprocess

duration_per_inattention = 60 * 1000  # 1 minute
duration_per_conc = 2 * 60 * 1000  # 2 minutes
# The  PVT  interstimulus interval varies randomly from 2 sec to 10 sec
min_interval = 2 * 1000
max_interval = 10 * 1000
header = ["Trial", "Start_Time", "Phase", "Cross_Trial_Start_Time", "Time_Wait", "Reaction_Time"]

# Create and initialize an Experiment
exp = design.Experiment("PVT")
control.defaults.window_mode = True
control.initialize(exp)
num_iterations = 5
tone = stimuli.Tone(1000)

# Define and preload standard stimuli
fixcross = stimuli.FixCross()
fixcross.preload()

exp.data_variable_names = ["current_time", "reaction_time"]
s_inattention = stimuli.BlankScreen()
s_inattention.preload()
s1_conc = stimuli.BlankScreen()
cross = stimuli.FixCross([100, 100])
cross.plot(s1_conc)
s1_conc.preload()


def design_experiment():
    print "design experiment"
    exp.data_variable_names = header
    for block_iter in range(num_iterations):
        b = design.Block()
        t_inattention = design.Trial()
        t_inattention.set_factor("phase", "inattention")
        b.add_trial(t_inattention)

        t_conc = design.Trial()
        t_conc.set_factor("phase", "concentrate")
        b.add_trial(t_conc)
        exp.add_block(b)


design_experiment()

# Start Experiment
control.start(skip_ready_screen=True)
stimuli.TextScreen("Instructions", "Test").present()
exp.keyboard.wait()

# display instructions for inattention phase, as well as blank screen
def run_inattention_trial(ind):
    phase = "inattention"
    print "inattention"
    stimuli.TextScreen(heading="Open your eyes but do not concentrate on the screen",
                       text="Press any key to continue").present()
    exp.keyboard.wait()
    s_inattention.present()
    trial_start_time = int(round(time.time() * 1000))
    exp.clock.wait(duration_per_inattention)
    tone.play()
    phase = trial.get_factor("phase")
    overall_band_data = [ind, trial_start_time, phase, 0, 0, 0]
    print overall_band_data
    exp.data.add(overall_band_data)


# display instructions for concentration phase, as well as cross at random interval and feedback of reaction time
def run_conc_trial(ind):
    phase = "concentrate"
    print "concentrate"
    stimuli.TextScreen(
        heading="A cross will appear on the screen at random intervals. Press any key immediately once it appears",
        text="Press any key to continue").present()
    exp.keyboard.wait()
    trial_start_time = int(round(time.time() * 1000))
    cross_trial_start_time = int(round(time.time() * 1000))
    print trial_start_time, duration_per_conc
    while cross_trial_start_time < trial_start_time + duration_per_conc:
        s_inattention.present()
        time_to_wait = random.randrange(min_interval, max_interval)
        print "time to wait: ", time_to_wait
        exp.clock.wait(time_to_wait)
        s1_conc.present()
        exp.clock.reset_stopwatch()
        c, time_taken = exp.keyboard.wait()
        reaction_time = exp.clock.stopwatch_time
        print "reaction time: ", reaction_time, time.time()
        exp.data.add([ind, trial_start_time, phase, str(cross_trial_start_time), time_to_wait, reaction_time])
        stimuli.TextScreen("Time taken", str(exp.clock.stopwatch_time) + "ms").present()

        exp.clock.wait(1500)
        cross_trial_start_time = int(round(time.time() * 1000))
    tone.play()


for iter_index, block in enumerate(exp.blocks):
    stimuli.TextScreen(heading="Iteration " + str(iter_index + 1), text="Press any key to continue").present()
    exp.keyboard.wait()
    for trial in block.trials:
        if trial.get_factor("phase") == "inattention":
            run_inattention_trial(iter_index)
        elif trial.get_factor("phase") == "concentrate":
            run_conc_trial(iter_index)
# End Experiment

control.end()
