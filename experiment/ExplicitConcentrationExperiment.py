#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Experiment displaying stimulus to prompt the user to concentrate, not concentrate (inattention) and close eyes (relax)

Consists of 3 different sections in each iteration: inattention (user stares at the screen without concentrating),
concentration(user imagines pushing he cross) and relax (user closes eyes)

This program only records the start time and end time of each trial. Actual collection of FFT bands and/or other data
 from headsets to be done in a separate program. Use timestamps to correspond the headset data to the experiment data
Requires: Python 2.7, Expyriment
See installation information: http://docs.expyriment.org/Installation.html

"""
from expyriment import design, control, stimuli
import time



def create_header():
    result = ["Trial", "Start_Time", "End_Time", "Phase"]
    # print result
    return result


header = create_header()
# Create and initialize an Experiment
exp = design.Experiment("Concentration Experiment")
control.defaults.window_mode = False
control.initialize(exp)

#in milliseconds
relax_time = 30000
conc_time = 20000
inattention_time = 10000

interval_btwn_readings = 500
num_iterations = 20
iter_index = 0
trial_start_time = 0

tone = stimuli.Tone(1000)


def design_experiment():
    exp.data_variable_names = header
    for block_iter in range(num_iterations):
        b = design.Block()
        t_inattention = design.Trial()
        t_inattention.set_factor("phase", "inattention")
        s_inattention = stimuli.BlankScreen()
        t_inattention.add_stimulus(s_inattention)
        b.add_trial(t_inattention)

        t_conc = design.Trial()
        t_conc.set_factor("phase", "concentrate")
        s1_conc = stimuli.BlankScreen()
        cross = stimuli.FixCross([100, 100])
        cross.plot(s1_conc)
        t_conc.add_stimulus(s1_conc)
        b.add_trial(t_conc)

        t_relax = design.Trial()
        t_relax.set_factor("phase", "relax")
        s1_relax = stimuli.BlankScreen()
        # t_relax.add_stimulus(s_relax)
        t_relax.add_stimulus(s1_relax)
        b.add_trial(t_relax)
        exp.add_block(b)


def countdown_stimuli():
    for counter in range(5, 0, -1):
        stimuli.TextScreen("Experiment is starting soon ", str(counter)).present()
        exp.clock.wait(1000)


design_experiment()
# Start Experiment
control.start(skip_ready_screen=True)

for block in exp.blocks:
    stimuli.TextScreen(heading="Iteration " + str(iter_index + 1), text="Press any key to continue").present()
    exp.keyboard.wait()
    for trial in block.trials:
        phase_time = 0
        if trial.get_factor("phase") == "inattention":
            # print "inattention"
            stimuli.TextScreen(heading="Open your eyes but do not concentrate on the screen",
                               text="Press any key to continue").present()
            phase_time = inattention_time
        elif trial.get_factor("phase") == "concentrate":
            # print "concentrate"
            stimuli.TextScreen(heading="Concentrate on the cross",
                               text="Press any key to continue").present()
            phase_time = conc_time
        else:
            # relax
            # print "relax"
            stimuli.TextScreen(heading="Rest period. Close your eyes", text="Press any key to continue").present()
            phase_time = relax_time
        exp.keyboard.wait()
        for index, trial_stimuli in enumerate(trial.stimuli):
            trial_stimuli.present()
            trial_start_time = int(round(time.time() * 1000))
            exp.clock.wait(phase_time)
            tone.play()
            trial_end_time = int(round(time.time() * 1000))
            phase = trial.get_factor("phase")
            overall_band_data = [iter_index, trial_start_time, trial_end_time, phase]
            # print overall_band_data
            exp.data.add(overall_band_data)
    iter_index += 1
