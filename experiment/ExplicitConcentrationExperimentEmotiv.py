#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Experiment to collect the FFT band power reading when the user is concentrating, not concentrating (inattention) and
eyes closed (relax) using the Emotiv Insight headset.

Consists of 3 different sections in each iteration: inattention (user stares at the screen without concentrating),
concentration(user imagines pushing he cross) and relax (user closes eyes)

Requires: Python 2.7, Expyriment
See installation information: http://docs.expyriment.org/Installation.html
"""
from expyriment import design, control, stimuli
import sys
import os
import platform
from array import array
from ctypes import *

try:
    if sys.platform.startswith('win32'):
        libEDK = cdll.LoadLibrary("..\\library\\edk.dll")
    else:
        raise Exception('System not supported.')
except Exception as e:
    print 'Error: cannot load EDK lib:', e
    control.end(goodbye_text="Cannot load EDK lib. Please press any key to exit", confirmation=True)

# configure functions
IEE_EmoEngineEventCreate = libEDK.IEE_EmoEngineEventCreate
IEE_EmoEngineEventCreate.restype = c_void_p

IEE_EmoEngineEventGetEmoState = libEDK.IEE_EmoEngineEventGetEmoState
IEE_EmoEngineEventGetEmoState.argtypes = [c_void_p, c_void_p]
IEE_EmoEngineEventGetEmoState.restype = c_int

IEE_EmoStateCreate = libEDK.IEE_EmoStateCreate
IEE_EmoStateCreate.restype = c_void_p

IS_GetContactQuality = libEDK.IS_GetContactQuality
IS_GetContactQuality.restype = c_int
IS_GetContactQuality.argtypes = [c_void_p, c_int]

alphaValue = c_double(0)
low_betaValue = c_double(0)
high_betaValue = c_double(0)
gammaValue = c_double(0)
thetaValue = c_double(0)

alpha = pointer(alphaValue)
low_beta = pointer(low_betaValue)
high_beta = pointer(high_betaValue)
gamma = pointer(gammaValue)
theta = pointer(thetaValue)

userID = c_uint(0)
user = pointer(userID)
composerPort = c_uint(1726)

channelList = array('I', [3, 7, 9, 12, 16])  # IED_AF3, IED_AF4, IED_T7, IED_T8, IED_Pz
channelDict = {3: 'AF3', 7: 'AF4', 9: 'T7', 12: 'T8', 16: 'PZ'}


def create_header():
    result = ["Trial", "Start_Time", "Current_Time", "Phase"]
    channel_specific_data = ["Theta", "Alpha", "Low_beta", "High_beta", "Gamma", "Contact_quality"]
    for channel in channelList:
        for band in channel_specific_data:
            result.append(band + "_" + str(channel))
    print
    result
    return result


header = create_header()

# Create and initialize an Experiment
exp = design.Experiment("Concentration Experiment")
control.defaults.window_mode = True
control.initialize(exp)

state = IEE_EmoStateCreate()
event = IEE_EmoEngineEventCreate()

version = -1
composerPort = c_uint(1726)
controlPanelPort = c_uint(3008)

userEngineID = c_uint(0)
userEngineIDP = pointer(userEngineID)

relax_time = 30000
conc_time = 20000
inattention_time = 10000

interval_btwn_readings = 500
num_iterations = 20
iter_index = 0
trial_start_time = 0

tone = stimuli.Tone(1000)


def connect_to_cloud():
    print
    "connect to cloud"
    if libEDK.IEE_EngineConnect("Emotiv Systems-5") != 0:
        print
        "Emotiv Engine start up failed."
        control.end(goodbye_text="Emotiv Engine start up failed. Please press any key to exit", confirmation=True)

        # if libEDK.IEE_EngineRemoteConnect("127.0.0.1", composerPort) != 0:
        #     control.end(goodbye_text="Emotiv Engine start up failed. Please press any key to exit", confirmation=True)
        #     exit()


def add_user():
    is_user_added = False
    while not is_user_added:
        error_code = libEDK.IEE_EngineGetNextEvent(event)
        if error_code == 0:
            event_type = libEDK.IEE_EmoEngineEventGetType(event)
            # by default, window type is hanning
            libEDK.IEE_EmoEngineEventGetUserId(event, userEngineIDP)
            if event_type == 16:  # libEDK.IEE_Event_enum.IEE_UserAdded
                print
                "User ID: %d have added" % userEngineID.value
                is_user_added = True


def design_experiment():
    print
    "design experiment"
    exp.data_variable_names = header
    for block_iter in range(num_iterations):
        b = design.Block()
        t_inattention = design.Trial()
        t_inattention.set_factor("phase", "inattention")
        s_inattention = stimuli.BlankScreen()
        t_inattention.add_stimulus(s_inattention)
        b.add_trial(t_inattention)

        t_conc = design.Trial()
        t_conc.set_factor("phase", "concentrate")
        s1_conc = stimuli.BlankScreen()
        cross = stimuli.FixCross([100, 100])
        cross.plot(s1_conc)
        t_conc.add_stimulus(s1_conc)
        b.add_trial(t_conc)

        t_relax = design.Trial()
        t_relax.set_factor("phase", "relax")
        s1_relax = stimuli.BlankScreen()
        # t_relax.add_stimulus(s_relax)
        t_relax.add_stimulus(s1_relax)
        b.add_trial(t_relax)
        exp.add_block(b)


def countdown_stimuli():
    for counter in range(5, 0, -1):
        stimuli.TextScreen("Experiment is starting soon ", str(counter)).present()
        exp.clock.wait(1000)


def evaluate():
    error_code = libEDK.IEE_EngineGetNextEvent(event)
    phase = trial.get_factor("phase")
    print
    error_code
    if error_code == 0:
        libEDK.IEE_EmoEngineEventGetEmoState(event, state)
        current_time = exp.clock.time
        overall_band_data = [iter_index, trial_start_time, current_time, phase]
        for i in channelList:
            contact_quality = IS_GetContactQuality(state, i)
            result = libEDK.IEE_GetAverageBandPowers(userID, i, theta, alpha, low_beta, high_beta, gamma)
            if result == 0:  # EDK_OK
                overall_band_data.extend([thetaValue.value, alphaValue.value,
                                          low_betaValue.value, high_betaValue.value, gammaValue.value, contact_quality])
            else:
                print
                "error"
                overall_band_data.extend([-1, -1, -1, -1, -1, contact_quality])

        print
        overall_band_data
        exp.data.add(overall_band_data)
    exp.clock.wait(interval_btwn_readings)


design_experiment()
connect_to_cloud()
add_user()
# Start Experiment
control.start(skip_ready_screen=True)

for block in exp.blocks:
    stimuli.TextScreen(heading="Iteration " + str(iter_index + 1), text="Press any key to continue").present()
    exp.keyboard.wait()
    for trial in block.trials:
        time = 0
        if trial.get_factor("phase") == "inattention":
            print
            "inattention"
            stimuli.TextScreen(heading="Open your eyes but do not concentrate on the screen",
                               text="Press any key to continue").present()
            time = inattention_time
        elif trial.get_factor("phase") == "concentrate":
            print
            "concentrate"
            stimuli.TextScreen(heading="Concentrate on the cross",
                               text="Press any key to continue").present()
            time = conc_time
        else:
            # relax
            print
            "relax"
            stimuli.TextScreen(heading="Rest period. Close your eyes", text="Press any key to continue").present()
            time = relax_time
        exp.keyboard.wait()
        for index, trial_stimuli in enumerate(trial.stimuli):
            trial_stimuli.present()
            trial_start_time = exp.clock.time
            exp.clock.wait(time, function=evaluate)
            tone.play()
    iter_index += 1
