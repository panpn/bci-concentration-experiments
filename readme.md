# Project overview
Various experiments to examine the differences in EEG activity (represented by FFT band powers) between concentration and inattention. Includes programs to collect data from the Emotiv Insight headset and Muse 2014 headset

## Primary project components
### experiment
ExplicitConcentrationExperiment.py: Experiment displaying stimulus to prompt the user to concentrate, not concentrate (inattention) and close eyes (relax)
ExplicitConcentrationExperimentEmotiv.py: Experiment to collect the FFT band power reading when the user is concentrating, not concentrating (inattention) and
eyes closed (relax). Data from the Emotiv Insight headset is collected simultaneously in the same file
PVT.py: An experiment that consists of 2 phases: inattention (blank screen ) and the Psychomotor vigilance task

### data-collector
LogDataMuse.py: Listens and collects data streamed from MuseIO, and saves them into a CSV file
LogDataEmotiv.py: Polls FFT band power data from Emotiv Insighe headset/Xavier composer and saves them into a CSV file

### library
edk.dll: library needed to connect to Emotiv Insight headset

### machine learning
ConcentrationMachineLearning.py: Contains functions to run various machine learning utilities

### processing
ProcessDataMuse.py: Combines experiment data recorded by PVT or Explicit Concentration Experiment with readings measured by LogDataMuse from the Muse 2014 headset

## Installation instructions
1. Install Anaconda from https://www.continuum.io/downloads
2. 2 Conda environments are needed: 1 with Python 3, and the other with python 2
3. In the Python 3 environment, install python-osc using pip (e.g. python -m pip install python-osc)
4. In the Python 2 environment, install expyriment v0.8.0 using pip (python -m pip install expyriment==0.8.0)

 
