'''
Listens and collects data streamed from MuseIO, and saves them into a CSV file
Run this command to listen to MuseIO
muse-io --dsp --device Muse-989D --osc osc.udp://localhost:5000 --osc-timestamp

Requires: Python 3.6 and python OSC library is needed
'''

import argparse, csv, time

# python 3 is needed
from pythonosc import dispatcher
from pythonosc import osc_server

csv_writer = None
header = ["current_time_ms", "type", "TP9", "FP1", "FP2", "TP10"]

#data collected in this file
channels_to_listen = ["/muse/eeg",
                      "/muse/elements/raw_fft0",
                      "/muse/elements/raw_fft1",
                      "/muse/elements/raw_fft2",
                      "/muse/elements/raw_fft3",
                      "/muse/elements/low_freqs_absolute",
                      "/muse/elements/delta_absolute",
                      "/muse/elements/theta_absolute",
                      "/muse/elements/alpha_absolute",
                      "/muse/elements/beta_absolute",
                      "/muse/elements/gamma_absolute",
                      "/muse/elements/horseshoe",
                      "/muse/eeg/quantization",
                      "/muse/batt",
                      "/muse/elements/experimental/concentration",
                      "/muse/elements/experimental/mellow",
                      ]

battery_level = 100
alpha_left = 0
alpha_right = 0
alpha_mid = 0
alpha_mid_2 = 0

def eeg_handler(unused_addr, args, *data):
    # [TP9, Fp1, Fp2, TP10]
    global battery_level, alpha_left, alpha_right, alpha_mid, alpha_mid_2
    current_time = int(round(time.time() * 1000))
    if args[0] == "/muse/batt":
       battery_level = list(data)[0]/100
    else:
        csv_writer.writerow([current_time, args[0]] + list(data))
        if args[0] == '/muse/elements/alpha_absolute':
            alpha_left = list(data)[0]
            alpha_mid = list(data)[1]
            alpha_mid_2 = list(data)[2]
            alpha_right = list(data)[3]
        elif args[0] == "/muse/elements/horseshoe":
            print(args[0], list(data), alpha_left, alpha_mid, alpha_mid_2, alpha_right, battery_level)
    # print(args[0], list(data))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip",
                        default="127.0.0.1",
                        help="The ip to listen on")
    parser.add_argument("--port",
                        type=int,
                        default=5000,
                        help="The port to listen on")
    args = parser.parse_args()
    dispatcher = dispatcher.Dispatcher()
    dispatcher.map("/debug",print)
    for channel in channels_to_listen:
        dispatcher.map(channel, eeg_handler, channel)
        print(channel, eeg_handler, channel)

    csv_writer = csv.writer(open('muse_data' + str(time.time()) + '.csv', 'w'), delimiter=' ', quotechar='|',
                            quoting=csv.QUOTE_MINIMAL)
    csv_writer.writerow(header)
    server = osc_server.ThreadingOSCUDPServer(
        (args.ip, args.port), dispatcher)
    print("Serving on {}".format(server.server_address))
    server.serve_forever()
