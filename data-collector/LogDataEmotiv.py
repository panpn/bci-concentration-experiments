'''
Polls FFT band power data from Emotiv Insighe headset/Xavier composer and saves them into a CSV file
Requires Python 2.7
'''

import sys, os, platform, time
from ctypes import *
from array import array
import csv

isConnectToHeadset = False
try:
    if sys.platform.startswith('win32'):
        libEDK = cdll.LoadLibrary("..\\library\\edk.dll")
    else:
        raise Exception('System not supported.')
except Exception as e:
    print 'Error: cannot load EDK lib:', e
    exit()

# configure functions
IEE_EmoEngineEventCreate = libEDK.IEE_EmoEngineEventCreate
IEE_EmoEngineEventCreate.restype = c_void_p

IEE_EmoEngineEventGetEmoState = libEDK.IEE_EmoEngineEventGetEmoState
IEE_EmoEngineEventGetEmoState.argtypes = [c_void_p, c_void_p]
IEE_EmoEngineEventGetEmoState.restype = c_int

IEE_EmoStateCreate = libEDK.IEE_EmoStateCreate
IEE_EmoStateCreate.restype = c_void_p

IS_GetContactQuality = libEDK.IS_GetContactQuality
IS_GetContactQuality.restype = c_int
IS_GetContactQuality.argtypes = [c_void_p, c_int]

alphaValue = c_double(0)
low_betaValue = c_double(0)
high_betaValue = c_double(0)
gammaValue = c_double(0)
thetaValue = c_double(0)

alpha = pointer(alphaValue)
low_beta = pointer(low_betaValue)
high_beta = pointer(high_betaValue)
gamma = pointer(gammaValue)
theta = pointer(thetaValue)

version = -1
composerPort = c_uint(1726)
controlPanelPort = c_uint(3008)

userEngineID = c_uint(0)
userEngineIDP = pointer(userEngineID)

userID = c_uint(0)
user = pointer(userID)
composerPort = c_uint(1726)
state = IEE_EmoStateCreate()
event = IEE_EmoEngineEventCreate()

channelList = array('I', [3, 7, 9, 12, 16])  # IED_AF3, IED_AF4, IED_T7, IED_T8, IED_Pz
channelDict = {3: 'AF3', 7: 'AF4', 9: 'T7', 12: 'T8', 16: 'PZ'}
interval_btwn_readings = 500


def create_header():
    result = ["Current_Time"]
    channel_specific_data = ["Theta", "Alpha", "Low_beta", "High_beta", "Gamma", "Contact_quality"]
    for channel in channelList:
        for band in channel_specific_data:
            result.append(band + "_" + str(channel))
    print result
    return result


def connect_to_headset():
    print "connect to headset"
    if libEDK.IEE_EngineConnect("Emotiv Systems-5") != 0:
        print "Emotiv Engine start up failed."
        exit()


def connect_to_composer():
    if libEDK.IEE_EngineRemoteConnect("127.0.0.1", composerPort) != 0:
        print "Emotiv Engine start up failed. Please press any key to exit"
        exit()


def add_user():
    is_user_added = False
    while not is_user_added:
        error_code = libEDK.IEE_EngineGetNextEvent(event)
        if error_code == 0:
            event_type = libEDK.IEE_EmoEngineEventGetType(event)
            # by default, window type is hanning
            libEDK.IEE_EmoEngineEventGetUserId(event, userEngineIDP)
            if event_type == 16:  # libEDK.IEE_Event_enum.IEE_UserAdded
                print "User ID: %d have added" % userEngineID.value
                is_user_added = True


if isConnectToHeadset:
    connect_to_headset()
else:
    connect_to_composer()
add_user()

with open('output.csv', 'wb') as csvfile:
    csv_writer = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    csv_writer.writerow(create_header())
    while True:
        error_code = libEDK.IEE_EngineGetNextEvent(event)
        # print error_code
        if error_code == 0:
            libEDK.IEE_EmoEngineEventGetEmoState(event, state)
            current_time = time.time()
            overall_band_data = [current_time]
            # collect the contact quality and FFT band power readings from each channel
            for i in channelList:
                contact_quality = IS_GetContactQuality(state, i)
                result = libEDK.IEE_GetAverageBandPowers(userID, i, theta, alpha, low_beta, high_beta, gamma)
                if result == 0:  # EDK_OK
                    overall_band_data.extend([thetaValue.value, alphaValue.value,
                                              low_betaValue.value, high_betaValue.value, gammaValue.value,
                                              contact_quality])
                else:
                    # print "error"
                    # -1 indicates that FFT band power data could not be obtained for that channel
                    overall_band_data.extend([-1, -1, -1, -1, -1, contact_quality])

            # print overall_band_data
            csv_writer.writerow(overall_band_data)
        csvfile.flush()
        time.sleep(0.5)
